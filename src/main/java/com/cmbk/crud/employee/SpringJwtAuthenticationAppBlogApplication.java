package com.cmbk.crud.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "com.cmbk.crud.employee" })
public class SpringJwtAuthenticationAppBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJwtAuthenticationAppBlogApplication.class, args);
	}

}
